package com.mf.driver.factory.mobile.remote;

import com.mf.driver.manager.mobile.remote.saucelab.SauceLabAndroidManager;
import com.mf.driver.manager.mobile.remote.saucelab.SauceLabIosManager;
import com.mf.driver.manager.web.remote.selenium.SeleniumGridChromeManager;
import com.mf.driver.manager.web.remote.selenium.SeleniumGridFirefoxManager;
import com.mf.enums.BrowserType;
import com.mf.enums.MobilePlatformType;
import org.openqa.selenium.WebDriver;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Supplier;

public final class SauceLabMobileFactory {

    private static final Map<MobilePlatformType, Supplier<WebDriver>> MAP = new EnumMap<>(MobilePlatformType.class);
    private static final Supplier<WebDriver> ANDROID = SauceLabAndroidManager::getDriver;
    private static final Supplier<WebDriver> IOS = SauceLabIosManager::getDriver;

    static {
        MAP.put(MobilePlatformType.ANDROID, ANDROID);
        MAP.put(MobilePlatformType.IOS, IOS);
    }

    private SauceLabMobileFactory() {
    }

    public static WebDriver getDriver(MobilePlatformType platformType) {
        return MAP.get(platformType).get();
    }
}
