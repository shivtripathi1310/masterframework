package com.mf.enums;

public enum BrowserType {

    CHROME,
    FIREFOX
}
