package com.mf.driver.factory.web.remote;

import com.mf.enums.BrowserRemoteType;
import com.mf.enums.BrowserType;
import org.openqa.selenium.WebDriver;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Function;

public final class RemoteDriverFactory {

    private static final Map<BrowserRemoteType, Function<BrowserType, WebDriver>> MAP = new EnumMap<>(BrowserRemoteType.class);
    private static final Function<BrowserType, WebDriver> SELENIUM = SeleniumGridFactory::getDriver;
    private static final Function<BrowserType, WebDriver> SELENOID = SelenoidFactory::getDriver;
    private static final Function<BrowserType, WebDriver> BROWSER_STACK = BrowserStackFactory::getDriver;

    static {
        MAP.put(BrowserRemoteType.SELENIUM, SELENIUM);
        MAP.put(BrowserRemoteType.SELENOID, SELENOID);
        MAP.put(BrowserRemoteType.BROWSER_STACK, BROWSER_STACK);
    }

    private RemoteDriverFactory() {
    }

    public static WebDriver getDriver(BrowserRemoteType browserRemoteType, BrowserType browserType) {

        return MAP.get(browserRemoteType).apply(browserType);
    }
}
