package com.mf.config.factory;

import com.mf.config.BrowserStackConfig;
import org.aeonbits.owner.ConfigCache;

public final class BrowserStackFactory {

    private BrowserStackFactory(){}

    public static BrowserStackConfig getConfig(){
        return ConfigCache.getOrCreate(BrowserStackConfig.class);
    }
}
