package com.mf.driver.factory.mobile.remote;

import com.mf.enums.MobilePlatformType;
import com.mf.enums.MobileRemoteModeType;
import org.openqa.selenium.WebDriver;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Function;

public final class RemoteMobileDriverFactory {

    private static final Map<MobileRemoteModeType, Function<MobilePlatformType, WebDriver>> MAP = new EnumMap<>(MobileRemoteModeType.class);
    private static final Function<MobilePlatformType, WebDriver> BROWSER_STACK = BrowserStackMobileFactory::getDriver;
    private static final Function<MobilePlatformType, WebDriver> SAUCE_LAB = SauceLabMobileFactory::getDriver;

    static {
        MAP.put(MobileRemoteModeType.BROWSER_STACK, BROWSER_STACK);
        MAP.put(MobileRemoteModeType.SAUCE_LAB, SAUCE_LAB);

    }

    private RemoteMobileDriverFactory() {
    }

    public static WebDriver getDriver(MobileRemoteModeType mobileRemoteModeType, MobilePlatformType mobilePlatformType) {

        return MAP.get(mobileRemoteModeType).apply(mobilePlatformType);
    }
}
