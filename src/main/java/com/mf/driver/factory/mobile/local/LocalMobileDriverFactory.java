package com.mf.driver.factory.mobile.local;

import com.mf.driver.manager.mobile.local.AndroidManager;
import com.mf.driver.manager.mobile.local.iOSManager;
import com.mf.enums.MobilePlatformType;
import org.openqa.selenium.WebDriver;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Supplier;

public final class LocalMobileDriverFactory {

    private static final Map<MobilePlatformType, Supplier<WebDriver>> MAP = new EnumMap<>(MobilePlatformType.class);
    private static final Supplier<WebDriver> ANDROID = AndroidManager::getDriver;
    private static final Supplier<WebDriver> IOS = iOSManager::getDriver;

    static {
        MAP.put(MobilePlatformType.ANDROID, ANDROID);
        MAP.put(MobilePlatformType.IOS, IOS);
    }

    private LocalMobileDriverFactory() {
    }

    public static WebDriver getDriver(MobilePlatformType mobilePlatformType) {
        return MAP.get(mobilePlatformType).get();
    }
}
