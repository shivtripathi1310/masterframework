package com.mf.enums;

public enum MobileRemoteModeType {

    BROWSER_STACK,
    SAUCE_LAB
}
