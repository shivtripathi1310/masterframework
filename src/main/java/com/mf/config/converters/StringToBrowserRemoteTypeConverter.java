package com.mf.config.converters;

import com.mf.enums.BrowserRemoteType;
import org.aeonbits.owner.Converter;

import java.lang.reflect.Method;

public class StringToBrowserRemoteTypeConverter implements Converter<BrowserRemoteType> {

    @Override
    public BrowserRemoteType convert(Method method, String remoteMode) {
        return BrowserRemoteType.valueOf(remoteMode.toUpperCase());
    }
}
