package com.mf.config;

import com.mf.config.converters.StringToBrowserRemoteTypeConverter;
import com.mf.config.converters.StringToBrowserTypeConverter;
import com.mf.config.converters.StringToRunModeBrowserTypeConverter;
import com.mf.config.converters.StringToURLConverter;
import com.mf.enums.*;
import org.aeonbits.owner.Config;

import java.net.URL;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "system:env",
        "file:${user.dir}/src/test/resources/config.properties"
})
public interface FrameworkConfig extends Config {

    @DefaultValue("CHROME")
    @ConverterClass(StringToBrowserTypeConverter.class)
    BrowserType browser();

    @Key("browserRemoteMode")
    @ConverterClass(StringToBrowserRemoteTypeConverter.class)
    BrowserRemoteType browserRemoteMode();

    @Key("runModeBrowser")
    @ConverterClass(StringToRunModeBrowserTypeConverter.class)
    RunModeBrowserType browserRunMode();

    @Key("seleniumGridURL")
    @ConverterClass(StringToURLConverter.class)
    URL seleniumGridURL();

    @Key("selenoidURL")
    @ConverterClass(StringToURLConverter.class)
    URL selenoidURL();

    @Key("localAppiumServerURL")
    @ConverterClass(StringToURLConverter.class)
    @DefaultValue("http://127.0.0.1:4723/wd/hub")
    URL localAppiumServerURL();

    @Key("mobileRemoteMode")
//    @ConverterClass(StringToBrowserRemoteTypeConverter.class)
    MobileRemoteModeType mobileRemoteMode();

    @Key("runModeMobile")
    @ConverterClass(StringToRunModeBrowserTypeConverter.class)
    RunModeMobileType runModeMobile();
}
