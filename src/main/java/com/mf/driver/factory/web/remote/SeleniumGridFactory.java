package com.mf.driver.factory.web.remote;

import com.mf.driver.manager.web.remote.selenium.SeleniumGridChromeManager;
import com.mf.driver.manager.web.remote.selenium.SeleniumGridFirefoxManager;
import com.mf.enums.BrowserType;
import org.openqa.selenium.WebDriver;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Supplier;

public final class SeleniumGridFactory {

    private static final Map<BrowserType, Supplier<WebDriver>> MAP = new EnumMap<>(BrowserType.class);
    private static final Supplier<WebDriver> CHROME = SeleniumGridChromeManager::getDriver;
    private static final Supplier<WebDriver> FIREFOX = SeleniumGridFirefoxManager::getDriver;

    static {
        MAP.put(BrowserType.CHROME, CHROME);
        MAP.put(BrowserType.FIREFOX, FIREFOX);
    }

    private SeleniumGridFactory() {
    }

    public static WebDriver getDriver(BrowserType browserType) {
        return MAP.get(browserType).get();
    }
}
