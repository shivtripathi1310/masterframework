package com.mf.driver.factory.mobile.remote;

import com.mf.driver.manager.mobile.remote.browserstack.BrowserStackAndroidManager;
import com.mf.driver.manager.mobile.remote.browserstack.BrowserStackIosManager;
import com.mf.enums.MobilePlatformType;
import org.openqa.selenium.WebDriver;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Supplier;

public final class BrowserStackMobileFactory {

    private static final Map<MobilePlatformType, Supplier<WebDriver>> MAP = new EnumMap<>(MobilePlatformType.class);
    private static final Supplier<WebDriver> ANDROID = BrowserStackAndroidManager::getDriver;
    private static final Supplier<WebDriver> IOS = BrowserStackIosManager::getDriver;

    static {
        MAP.put(MobilePlatformType.ANDROID, ANDROID);
        MAP.put(MobilePlatformType.IOS, IOS);
    }

    private BrowserStackMobileFactory() {
    }

    public static WebDriver getDriver(MobilePlatformType platformType) {
        return MAP.get(platformType).get();
    }
}
