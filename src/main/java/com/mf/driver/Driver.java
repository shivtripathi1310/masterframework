package com.mf.driver;

import com.mf.config.factory.ConfigFactory;
import com.mf.driver.factory.web.local.LocalDriverFactory;
import com.mf.driver.factory.web.remote.RemoteDriverFactory;
import com.mf.enums.RunModeBrowserType;
import org.openqa.selenium.WebDriver;

public class Driver {

    static WebDriver driver = null;

    public static WebDriver initDriver() {
        RunModeBrowserType runModeBrowserType = ConfigFactory.getConfig().browserRunMode();
        if (runModeBrowserType == RunModeBrowserType.LOCAL) {
            driver = LocalDriverFactory.getDriver(ConfigFactory.getConfig().browser());
        } else if (runModeBrowserType == RunModeBrowserType.REMOTE) {
            driver = RemoteDriverFactory.getDriver(ConfigFactory.getConfig().browserRemoteMode(),ConfigFactory.getConfig().browser());
        }
        return driver;
    }

    public static void quitDriver() {
        driver.quit();
    }
}
