package com.mf.tests;

import com.mf.driver.Driver;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class WebBase {

    protected WebDriver driver;

    @BeforeMethod
    public void setUp(){
        driver = Driver.initDriver();
    }

    @AfterMethod
    public void quitDriver(){
        Driver.quitDriver();
    }
}
